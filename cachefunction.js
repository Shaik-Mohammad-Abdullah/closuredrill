function cacheFunction(cb) {
	const cacheObject = {};
	return function (n) {
		console.log("Before cache");
		console.log(cacheObject);
		if (cacheObject[n]) {
			console.log("Inside if");
			return cacheObject[n];
		} else {
			console.log("Inside else");
			const value = cb(n);
			cacheObject[n] = value;
			return value;
		}
	};
};

const cb = n => {
	return n * 2;
};

module.exports = { cacheFunction, cb };