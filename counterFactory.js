function counterFactory() {
	let counter = 1;
	return {
		increment: function () {
			counter += 1;
			return counter;
		},
		decrement: function () {
			counter -= 1;
			return counter;
		}
	};
}

module.exports = counterFactory;
