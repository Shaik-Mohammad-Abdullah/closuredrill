function limitFunctionCallCount(cb, n) {
	let count = 1;
	return function (n) {
		if (count <= n) {
			count++;
			return cb(n);
		}
	};
}

const cb = (n) => n;

module.exports = { limitFunctionCallCount, cb };
