const cache = require('../cachefunction');

const invokeFunction = cache.cacheFunction(cache.cb);
console.log(invokeFunction(2));
console.log(invokeFunction(3));
console.log(invokeFunction(4));
console.log(invokeFunction(5));
console.log(invokeFunction(6));
console.log(invokeFunction(5));
console.log(invokeFunction(4));
